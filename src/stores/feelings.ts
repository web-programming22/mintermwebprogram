import { ref, computed } from "vue";
import { defineStore } from "pinia";
import type feeling from "@/feeling";
export const usefelingStore = defineStore("feelings", () => {
  const feelings = ref<feeling[]>([
    {
      id: 1, name: "Angry", img: "../../../feeling_imgs/angry.png",
    },
    {
      id: 2, name: "Cute", img: "../../../feeling_imgs/cute.png",
    },
    {
      id: 3, name: "Excited", img: "../../../feeling_imgs/excited.png",
    },
    {
      id: 4, name: "Pain", img: "../../../feeling_imgs/pain.png",
    },
    {
      id: 5, name: "Shocked", img: "../../../feeling_imgs/shocked.png",
    },
    {
      id: 6, name: "Smile", img: "../../../feeling_imgs/smile.png",
    },
  ]);

  return { feelings, };
});
